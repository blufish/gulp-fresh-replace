/**
 *  @author   Fernando Salazar <fernando@blufish.com>
 *  @author   James Gov <james@blufish.com>
 *  @since    Tuesday, September 07, 2016
 */

"use strict";

var through = require('through2'),
	concat  = require('concat-stream'),
	gUtil   = require('gulp-util'),
	fs      = require('fs'),
	path    = require('path');

module.exports = function(opts) {
	var _pattr    = '%%path:',
		_regex    = /%%path:\w+/g,

		_depthLimit  = 30,
		_depthCount  = 0,
		_matchCount  = 0,
		_parsedCount = 0,
		_views       = {},

		_config;

	/**
	 *  @param    object file, string encoding, function cb
	 *  @return   void
	 */
	function fresh(file, encoding, cb) {
		switch(true) {

			/**
			 *  Pass along null file.
			 */
			case file.isNull():
				cb(null, file);
				break;

			/**
			 *  @todo   support stream
			 */
			case file.isStream():
				file.contents.pipe(concat(function(data) {
					try {
						file.contents = new Buffer(_clean(String(data)));

						cb(null, file);
					} catch(e) {
						cb(new gUtil.PluginError('gulp-fresh', e.message));
					}
				}));
				break;

			/**
			 *  Process file
			 */
			case file.isBuffer():
				try {
					_config = JSON.parse(file.contents);

					var holder = [],
						i;

					Object.keys(_config).forEach(function(key) {
						var basePath = _config[key].includes.base;

						Object.keys(_config[key].includes).forEach(function(view){
							if(view !== 'base'){
								_views[view] = _config[key].includes[view];
							}
						});

						var base = fs.readFileSync(basePath, "utf8"),
							page;

						page = _parse(base, _views);

						var file = new gUtil.File({
								base: path.join(__dirname, './dist'),
								cwd: __dirname,
								path: path.join(__dirname, './'+_config[key].dist+'/index.html'),
								contents: new Buffer(String(page))
							});

						holder.push(file);
					});


					for(i=0; i<holder.length; i++){
						this.push(holder[i]);
					}

					cb(null, file);
				} catch (e) {
					cb(new gUtil.PluginError('gulp-fresh', e.message));
				}

				break;
		}

	}

	/**
	 *  @param    string content, function handle
	 *  @return   string
	 */
	function _parse(content) {
		var matches = content.match(_regex),
			i, key;

		_depthCount ++;

		if(_depthCount >= _depthLimit)
			throw new Error('Depth Limit Reached');

		if(matches === null) {
			return content;
		} else {
			for(i = 0; i < matches.length; i++) {
				_matchCount ++;

				key = matches[i].replace(_pattr, '');

				if(typeof _views[key] === 'undefined') {
					throw new Error('Path Key Not Found: ' + key);
				} else if((!opts) || (opts[key] === null)) {
					_parsedCount ++;
					return content.replace(matches[i], '');
				} else {
					_parsedCount ++;

					content = content.replace(matches[i], fs.readFileSync(_views[key], 'utf-8'));

					if(_matchCount >= _parsedCount)
						return _parse(content);
				}
			}
		}
	}

	return through.obj(fresh);
}
